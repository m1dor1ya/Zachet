<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <head>
            </head>
            <body>
                <h1>
                    Music events
                </h1>
                <br/>
                <h2>
                    There will be
                    <xsl:value-of select="count(//event)"/>
                     events on the next week
                    <xsl:apply-templates select="events"/>
                </h2>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="events/event">
        <div>
            <p>
                Who? <xsl:value-of select="@man"/>.
                <br/>
                When? <xsl:value-of select="@date"/>.
                <br/>
                Where? <xsl:value-of select="@place"/>.
            </p>
            <hr/>
        </div>
    </xsl:template>

</xsl:stylesheet>